/* Since the values are sorted in increasing order, the solution is to
 traverse tree in post-order, and when visiting a node, print the
 value at that node */
void btree_print_decreasing(Ttree tree)
{
  if (tree != NULL)
    {
      btree_print_decreasing(tree->right);
      printf("%i\n", tree->val);
      btree_print_decreasing(tree->left);
    }
}
