#include <stdio.h>

int main (void)
{
  int i;
  for (i = 1; i <= 100; ++i)
  {
    int mult3 = i % 3 == 0;
    int mult5 = i % 5 == 0;
    if (mult3 && mult5)
      printf("FizzBuzz\n");
    else if (mult3)
      printf("Fizz\n");
    else if (mult5)
      printf("Buzz\n");
    else
      printf("%i\n", i);
  }
  return 0;
}
