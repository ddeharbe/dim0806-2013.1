#include <stdlib.h>
unsigned list_length(Tlist list)
{
  unsigned res = 0;
  Tlist ptr = list;
  while (ptr != NULL) {
    res = res + 1;
    ptr = ptr->next;
  }
  return res;
}
