#include <stdlib.h>

Tlist list_reverse (Tlist list)
{
  if (list == NULL)
    return NULL;
  else
    {
      /* algorithm: make every item point to the item that was
	 pointing at it in the original list */
      Tlist rest = list;   /* items still to be reversed */
      Tlist result = NULL; /* items already reversed */
      /* insert first element of rest as the head of result */
      do
	{
	  Tlist first = rest;
	  rest = first->next;
	  first->next = result;
	  result = first;
	}
      while (rest != NULL);
      return result;
    }
}
