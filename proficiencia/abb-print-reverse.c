#include <stdio.h>

typedef struct node {
  struct node * left;    /* all values smaller than val */
  struct node * right;   /* all values greater than val */
  int           val;
} * Ttree;
