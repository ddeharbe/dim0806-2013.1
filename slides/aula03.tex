\documentclass[handout]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[brazil,british]{babel}
\usetheme{default} 
\usecolortheme{beaver}
\usepackage{graphicx}
\usepackage{clrscode}
\usepackage{hyperref}
% \usecolortheme{dove}
\title{Aula 03: Prática de Análise de Correção de Algoritmos}
\author{David Déharbe \\
  Programa de Pós-graduação em Sistemas e Computação \\
  Universidade Federal do Rio Grande do Norte \\
  Centro de Ciências Exatas e da Terra \\
  Departamento de Informática e Matemáica Aplicada}
\date{22 de fevereiro de 2013}
\logo{\includegraphics[width=1cm]{img/logo-ppgsc-icon-text.png}}

\newcommand{\sel}[1]{\textcolor{teal}{#1}}

\begin{document}
\selectlanguage{brazil}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Plano da aula}
  \tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}

  \frametitle{Objetivos}

  \begin{description}
  \item[Rever] a lógica de Hoare como ferramenta de verificação de algoritmos;
  \item[Fixar] o conceito de invariante de laço;
  \item[Acompanhar] a verificação completa de um algoritmo elementar;
  \item[Executar] a verificação completa de um algoritmo elementar;
  \item[Preparar] a verificação de algoritmos mais complexos.
  \end{description}

\end{frame}

\section{Revisão da lógica de Hoare}

\begin{frame}

\frametitle{Lógica de Hoare}

\[
  \{P \} C \{Q\}
\]

\begin{itemize} 
\item cada vez que o estado do programa satisfaz a pré-condição $P$,
\item quando o comando $C$ é executado, 
\item o estado do programa passa a satisfazer a pós-condição $Q$.
\end{itemize}

\begin{block}{Exemplo de tripla válida:}
\[
\{ x = 3 \} x \gets x + 1 \{ x = 4 \}
\]
\end{block}

\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

Uma regra para cada tipo de comando:
\begin{description}
  \item[vácuo] $\vdash \{P\} \kw{Skip} \{P\}$
  \item[atribuição]  $\vdash \{P \lbrack E / x \rbrack\} x \gets E \{P\}$
  \item[sequência] $\{P\} C_1 \{Q\}, \{Q\} C_2 \{R\} \vdash \{P\} C_1 ; C_2 \{R\}$
  \item[condicional] $\begin{array}[t]{rl}
      & \{ B \land P \} C_1 \{Q\}, \{ \neg B \land P \} C_2 \{Q\} \\
      \vdash & \{P\} \kw{If} \, B \, \kw{Then} \, C_1 \, \kw{Else} \, C_2 \, \{Q\}
      \end{array}$
  \item[laço] $\{ P \land G\} C \{P\} \vdash \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$
  \item[laço (com término)] 
    $\begin{array}[t]{rl}
      & \prec \mbox{ é uma ordem bem fundada}, \\
      & \lbrack P \land G \land t = z \rbrack C \lbrack P \land t \prec z \rbrack \\
      \vdash & \lbrack P \rbrack \kw{While}\, G \, \kw{Do} \, C \lbrack \neg G \land P \rbrack
    \end{array}$
\end{description}
Regras usadas para facilitar as verificações:
\begin{description}
  \item[fortalecimento pré-condição] $P' \Rightarrow P, \{ P \} C \{Q\} \vdash \{P'\} C \{Q\}$
  \item[enfraquecimento pós-condição] $\{ P \} C \{Q\}, Q \Rightarrow Q' \vdash \{P\} C \{Q'\}$
\end{description}
\end{frame}

\begin{frame}

\frametitle{Relação entre tripla de Hoare e correção de algoritmo?}

\begin{block}{Exemplo}
É válida a tripla de Hoare seguinte?
\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \}$
\li $j \gets 1$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\zi \Comment $\{ \begin{array}[t]{l}
      \left(1 \le r \le n \Leftrightarrow a_r = v\right) \land \\
      \left(r = \const{nil} \Leftrightarrow \left(\forall i : 1 \le i \le n \Rightarrow a_i \neq v \right)\right) \}
      \end{array}$
\end{codebox}  

\end{block}

\end{frame}

\section{Estratégias de verificação com a lógica de Hoare}

\begin{frame}
\frametitle{Aplicação à verificação de algoritmos: estratégia}

\begin{block}{Estratégia de divisão e conquista}
\begin{itemize}
\item Construir a prova por decomposição do objetivo em sub-objetivos
  mais simples, utilizando as regras da lógica de Hoare.
\item Provar os sub-objetivos.
\end{itemize}
\end{block}

\begin{block}{Tática}
\begin{itemize}
\item Identificar asserções que caracterizam os dados do algoritmo em cada
  ponto;
\item Inserir essas asserções intermediárias;
\item Combinar essas asserções em triplas de Hoare;
\item Aplicar as regras da lógica de Hoare para 
\begin{itemize}
\item comprovar que as asserções são válidas.
\item compor triplas consecutivas com pós e pré diferentes mas relacionadas por implicação.
\end{itemize}
\end{itemize}
\end{block}
\end{frame}

\section{Um exemplo básico}

\begin{frame}
\frametitle{Exemplo de verificação: apresentação}

\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I}: a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\li $y \gets 1$
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\li   $x \gets x+1$
    \End
\zi \Comment \textcolor{blue}{$\{ \mathcal{O}: y = a^n \}$}
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: decomposição}

Inserir asserções intermediárias e decompor em sub-objetivos:
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I}: a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{red}{$\{ P_1: \cdots \}$}
\li $y \gets 1$
\zi \Comment \textcolor{red}{$\{ P_2: \cdots \}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\li   $x \gets x+1$
    \End
\zi \Comment \textcolor{blue}{$\{ \mathcal{O}: y = a^n \}$}
\end{codebox}  
\pause
$$
\begin{array}{rcl}
  P_1 & : & \mathcal{I} \land x = 0 \\
  P_2 & : & \mathcal{I} \land x = 0 \land y = 1
\end{array}
$$

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: decomposição}

Asserções intermediárias
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I}: a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{ P_1: a \neq 0 \land n \ge 0 \land x = 0 \}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{ P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1 \}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\li   $x \gets x+1$
    \End
\zi \Comment \textcolor{blue}{$\{ \mathcal{O}: y = a^n \}$}
\end{codebox}  
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: decomposição}

Sub-objetivos obtidos aplicando a lei da sequência:
\begin{enumerate}
\item $\textcolor{blue}{\{ \mathcal{I}: a \neq 0 \land n \ge 0 \}} x \gets 0
\textcolor{blue}{\{ P_1: a \neq 0 \land n \ge 0 \land x = 0 \}}$
\item $\begin{array}[t]{l}
\textcolor{blue}{\{ P_1: a \neq 0 \land n \ge 0 \land x = 0\}} \\
\quad y \gets 1 \\
\textcolor{blue}{\{ P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}}
\end{array}$
\item $\begin{array}[t]{l}
\textcolor{blue}{\{ P_1: a \neq 0 \land n \ge 0 \land x = 0 \}} \\
\quad \kw{While} \, x < n \, \kw{Do} \, y \gets y \times a ; x \gets x+1 \\
\textcolor{blue}{\{ \mathcal{O}: y = a^n \}}
\end{array}$
\end{enumerate}  
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: sub-objetivo 1}

$\textcolor{blue}{\{ \mathcal{I}: a \neq 0 \land n \ge 0 \}} x \gets 0 \textcolor{blue}{\{ P_1: a \neq 0 \land n \ge 0 \land x = 0 \}}$ é válido?

\begin{itemize}
\item Lei da atribuição: $\begin{array}[t]{ll}
P_1 \lbrack 0 / x \rbrack & \iff \\
a \neq 0 \land n \ge 0 \land x = 0 \lbrack 0 / x \rbrack & \iff  \\
a \neq 0 \land n \ge 0 \land 0 = 0 & \iff \\
a \neq 0 \land n \ge 0 \land true & \iff \\
a \neq 0 \land n \ge 0 & \iff \\
\mathcal{I}. & \end{array}$
\end{itemize} 
\pause Sub-objetivo 1 é válido.
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: sub-objetivo 2}

$\begin{array}{l}
\textcolor{blue}{\{ P_1: a \neq 0 \land n \ge 0 \land x = 0 \}} \\
\quad y \gets 1 \\
\textcolor{blue}{\{ P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}}
\end{array}$ é válido?

\begin{itemize}
\item Lei da atribuição: $\begin{array}[t]{ll}
P_2 \lbrack 1 / y \rbrack & \iff \\
a \neq 0 \land n \ge 0 \land x = 0 \land y = 1 \lbrack 1 / y \rbrack & \iff  \\
a \neq 0 \land n \ge 0 \land x = 0 \land 1 = 1 & \iff \\
a \neq 0 \land n \ge 0 \land x = 0 \land true & \iff \\
a \neq 0 \land n \ge 0 \land x = 0 & \iff \\
P_1. & \end{array}$
\end{itemize} 
\pause Sub-objetivo 2 é válido.
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: sub-objetivo 3}

$$\begin{array}{l}
\textcolor{blue}{\{ P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}} \\
\quad \kw{While} \, x < n \, \kw{Do} \, y \gets y \times a ; x \gets x+1 \\
\textcolor{blue}{\{ \mathcal{O}: y = a^n \}}
\end{array}$$ 
é válido?

\pause
\begin{itemize}
\item Como usar a regra do laço para prosseguir? 
\item $\{ P \land G\} C \{P\} \vdash \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$ 
\item $G$ é $x < n$. 
\item Se $P$ for $P_2$, então $\mathcal{O} \neq \neg G \land P$.
\end{itemize}

\end{frame}

\subsection{Verificação na presença de laços}

\begin{frame}
\frametitle{Solução para tratar laços}

\begin{block}{Ideia}
Para provar $\{Q\}  \kw{While}\, G \, \kw{Do} \, C \, \{R\}$ encontrar um $P$ tal que
\begin{itemize}
\item $Q \Rightarrow P$
\item $P \land \neg G \Rightarrow R$
\end{itemize}
\end{block}

\begin{block}{Justificativa}
\begin{itemize}
\item Regra do laço: $\{ P \land G\} C \{P\} \vdash \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$ (sub-objetivo 3.1)
\item Fortalecimento da pré-condição (sub-objetivo 3.2): $$Q \Rightarrow P, \{P\} W \{P'\} \vdash \{Q\} W \{\neg G \land P\}.$$
\item Enfraquecimento da pós-condição (sub-objetivo 3.3): $$\{Q\} W \{\neg G \land P\}, \neg G \land P \Rightarrow R \vdash \{Q\} W \{R\}.$$
\item Como encontar $P$, o \emph{invariante} do laço?
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Asserções e laços}

A execução passa várias vezes pelo mesmo ponto em um estado diferente, e só há
uma asserção...
\begin{codebox}
\zi \Comment \textcolor{red}{$\varphi_1$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$ \textcolor{red}{$\{\varphi_2\}$} $x \gets x+1$ \textcolor{red}{$\{\varphi_3\}$}
    \End
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Aplicação à verificação de algoritmos: a regra do laço}

\begin{block}{Regra do laço}
$\{ P \land G\} C \{P\} \vdash \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$ 
\end{block}

\begin{itemize}
\item $P$ é o \emph{invariante} do laço;
\item deve ser verdadeiro imediatamente antes e depois de executar o laço;
\item caracteriza o estado cada vez que a guarda $G$ é avaliada;
\item deve decorrer (ser uma consequência lógica) do código que precede
  o laço;
\item deve ser suficiente para verificar que o resto do algoritmo, que segue o laço, é correto;
\item a execução do corpo do laço $C$ deve mantê-lo verdadeiro;
\pause
\item EXEMPLO!
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação}

\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{red}{$P_1$}
\li $y \gets 1$
\zi \Comment \textcolor{red}{$P_2$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\zi \Comment \textcolor{red}{$P_3$}
\li   $x \gets x+1$
\zi \Comment \textcolor{red}{$P_4$}
    \End
\zi \Comment \textcolor{blue}{$\{ y = a^n \}$}
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{P_1: a \neq 0 \land n \ge 0 \land x = 0\}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\zi \Comment \textcolor{red}{$P_3$}
\li   $x \gets x+1$
\zi \Comment \textcolor{red}{$P_4$}
    \End
\zi \Comment \textcolor{blue}{$\{ y = a^n \}$}
\end{codebox}  
\end{minipage}
&
\pause
\begin{minipage}{.4\textwidth}
$$
\begin{array}{lcc}
    & x & y \\
\hline
\pause
P_1 & 0 & - \\
\pause
P_2 & 0 & 1 \\
\pause
P_3 & 0 & a \\
\pause
P_4 & 1 & a \\
\pause
P_3 & 1 & a^2 \\
\pause
P_4 & 2 & a^2 \\
\pause
P_3 & 2 & a^3 \\
\pause
P_4 & 3 & a^3 \\
\pause
P_3 & 3 & a^4 \\
\pause
P_4 & 4 & a^4 \\
\pause
\vdots & \vdots & \vdots \\
P_3 & n-1 & a^n \\
P_4 & n & a^n \\
\end{array}
$$
\end{minipage}
\end{tabular}

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{P_1: a \neq 0 \land n \ge 0 \land x = 0\}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\zi \Comment \textcolor{red}{$P_3$}
\li   $x \gets x+1$
\zi \Comment \textcolor{red}{$P_4$}
    \End
\zi \Comment \textcolor{blue}{$\{ y = a^n \}$}
\end{codebox}  
\end{minipage}
&
\begin{minipage}{.4\textwidth}
$$
\begin{array}{lcc}
    & x & y \\
\hline
P_1 & 0 & - \\
\sel{P_2} & \sel{0} & \sel{1} \\
P_3 & 0 & a \\
\sel{P_4} & \sel{1} & \sel{a} \\
P_3 & 1 & a^2 \\
\sel{P_4} & \sel{2} & \sel{a^2} \\
P_3 & 2 & a^3 \\
\sel{P_4} & \sel{3} & \sel{a^3} \\
P_3 & 3 & a^4 \\
\sel{P_4} & \sel{4} & \sel{a^4} \\
\vdots & \vdots & \vdots \\
P_3 & n-1 & a^n \\
\sel{P_4} & \sel{n} & \sel{a^n} \\
\end{array}
$$
\end{minipage}
\end{tabular}

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{P_1: a \neq 0 \land n \ge 0 \land x = 0\}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\zi \Comment \textcolor{red}{$P_3$}
\li   $x \gets x+1$
\zi \Comment \textcolor{red}{$P_4$}
    \End
\zi \Comment \textcolor{blue}{$\{ y = a^n \}$}
\end{codebox}  
\end{minipage}
&
\begin{minipage}[t]{.4\textwidth}
$$
\begin{array}{lcc}
    & x & y \\
\hline
P_1 & 0 & - \\
\sel{P_2} & \sel{0} & \sel{1} \\
P_3 & \multicolumn{2}{c}{0 \le x < n \land y = a^{x+1}} \\
P_4 & \multicolumn{2}{c}{1 \le x \le n \land y = a^x}
\end{array}
$$
\end{minipage}
\end{tabular}

\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\zi \ldots
\zi \Comment \textcolor{blue}{$\{P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\zi \Comment \textcolor{red}{$P_3$}
\li   $x \gets x+1$
\zi \Comment \textcolor{red}{$P_4$}
    \End
\zi \ldots
\end{codebox}  
\end{minipage}
&
\begin{minipage}[t]{.4\textwidth}
$$
\begin{array}{lcc}
    & x & y \\
\hline
P_1 & 0 & - \\
\sel{P_2} & \sel{0} & \sel{1} \\
P_3 & \multicolumn{2}{c}{0 \le x < n \land y = a^{x+1}} \\
P_4 & \multicolumn{2}{c}{1 \le x \le n \land y = a^x}
\end{array}
$$
\end{minipage}
\end{tabular}

\begin{itemize}
\item $P_4$ é verdadeiro ao fim de cada iteração \pause
\item $P_4$ não é verdadeiro antes do laço \pause
\item Mas $\textcolor{red}{0} \le x \le n \land y = a^x$  o é! \pause
\item Será que é este o invariante? 
\item Se for o invariante, o algoritmo é correto?
\end{itemize}
\end{frame}

\begin{frame}

\frametitle{Exemplo de verificação}

$P_4$ é invariante do laço?

ou seja: a seguinte tripla é válida? (definição)
\[
\{ \underbrace{P}_{0 \le x \le n \land y = a^x} \land \underbrace{G}_{x < n}\} \underbrace{C}_{y \gets y \times a; x \gets x+1} \{\underbrace{P}_{0 \le x \le n \land y = a^x}\}
\]
ou seja: a seguinte tripla é válida? (instanciação)
\[
\{ 0 \le x \le n \land y = a^x \land x < n \} y \gets y \times a; x \gets x+1 \{0 \le x \le n \land y = a^x \}
\]
Para responder, podemos
\begin{itemize}
\item simplificar:
 $\{ 0 \le x < n \land y = a^x \} y \gets y \times a; x \gets x+1 \{0 \le x \le n \land y = a^x \}$;
\item aplicar a regra da sequência (possivelmente usando $P_3$).
\end{itemize}
\end{frame}

\begin{frame}

\frametitle{Exemplo de verificação}

Devemos encontrar $Q$ tal que:
\[
\begin{array}{l}
\{ 0 \le x < n \land y = a^x \} y \gets y \times a \{ Q \}, \mbox{ e}\\
\{ Q \} x \gets x+1 \{0 \le x \le n \land y = a^x \}
\end{array}
\]
\pause
(obs. $Q$ deveria corresponder a $P_3$)
\pause
\begin{itemize}
\item Pela regra da atribuição, verificamos que a segunda tripla é tal que:
\begin{array}[t]{rcl} 
Q & \equiv & 0 \le x \le n \land y = a^x \lbrack x+1 / x \rbrack\\
  & \iff & 0 \le x+1 \le n \land y = a^{x+1} \\
  & \iff & -1 \le x < n \land y = a^{x+1} \\
  & \Rightarrow & P_3
\end{array}
\end{itemize}

\end{frame}

\begin{frame}

\frametitle{Exemplo de verificação}

Substituímos $Q$ pela fórmula encontrada ($P_3$) na primeira tripla e devemos
verificar se a tripla encontrada é válida:
\[
\begin{array}{l}
\{ 0 \le x < n \land y = a^x \} y \gets y \times a \{ -1 \le x < n \land y = a^{x+1} \}?
\end{array}
\]
Pela regra da atribuição temos que a seguinte tripla é válida:
\[
\begin{array}{l}
\{ -1 \le x < n \land y \times a = a^{x+1} \} y \gets y \times a \{ -1 \le x < n \land y = a^{x+1} \}
\end{array}
\]
Observamos que $1 \le x \le n \land x < n \Rightarrow -1 \le x < n$ e podemos
aplicar a regra do enfraquecimento da pré-condição:
$
\begin{array}[t]{rl}
& \left(0 \le x \le n \land y = a^x \land x < n\right) \Rightarrow 
 \left(-1 \le x < n \land y \times a = a^{x+1}\right), \\
& \{ -1 \le x < n \land y \times a = a^{x+1} \} y \gets y \times a \{ 0 \le x < n \land y = a^{x+1} \} \\
\vdash &
\{ 0 \le x \le n \land y = a^x \land x < n \} y \gets y \times a \{ 0 \le x < n \land y = a^{x+1} \}
\end{array}$
\qed
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: laço}

Provamos que a seguinte tripla é válida:
\[
\{ 0 \le x \le n \land y = a^x \land x < n \} y \gets y \times a; x \gets x+1 \{0 \le x \le n \land y = a^x \}
\]
O algoritmo é correto?
\pause
\begin{itemize}
\item A regra do laço é:
\[
\{ P \land G\} C \{P\} \vdash \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}
\]
\item Logo, mostramos que a tripla seguinte é válida:
\[
\begin{array}[t]{l}
\{0 \le x \le n \land y = a^x\} \\
\quad \kw{While}\,x < n\,\kw{Do}\, y \gets y \times a; x \gets x+1\, \\
\{0 \le x \le n \land y = a^x \land \neg x < n\}.
\end{array}
\]
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: recapitulando e fechando}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ a \neq 0 \land n \ge 0 \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{P_1: a \neq 0 \land n \ge 0 \land x = 0\}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{P_2: a \neq 0 \land n \ge 0 \land x = 0 \land y = 1\}$}
\zi \Comment \textcolor{blue}{$\{P': 0 \le x \le n \land y = a^x\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\li   $x \gets x+1$
    \End
\zi \Comment \textcolor{blue}{$\{P' \land \neg x < n\}$}
\zi \Comment \textcolor{blue}{$\{ y = a^n \}$}
\end{codebox}  
\end{minipage}
&
\begin{minipage}{.4\textwidth}
\begin{itemize}
\item fortalecimento pré-condição: $P_2 \Rightarrow P'$
\item enfraquecimento pós-condição: $P' \land \neg x < n \Rightarrow y = a^n$
\end{itemize}  
\end{minipage}
\end{tabular}
\end{frame}

\begin{frame}
\frametitle{Exemplo de verificação: resumo}

\begin{tabular}{ll}
\begin{minipage}{.55\textwidth}
\begin{codebox}
\Procname{$\proc{Power}(a, n)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I} \}$}
\li $x \gets 0$
\zi \Comment \textcolor{blue}{$\{P_1: ...\}$}
\li $y \gets 1$
\zi \Comment \textcolor{blue}{$\{P_2: ...\}$}
\zi \Comment \textcolor{blue}{$\{P': ...\}$}
\li \While $x < n$
\li \Do
\li   $y \gets y \times a$
\li   $x \gets x+1$
    \End
\zi \Comment \textcolor{blue}{$\{P' \land \neg G\}$}
\zi \Comment \textcolor{blue}{$\{\mathcal{O} \}$}
\end{codebox}  
\end{minipage}
&
\begin{minipage}{.4\textwidth}
\begin{itemize}
\item $\{\mathcal{I}\} x \gets 0 \{ P_1 \}$
\item $\{P_1\} y \gets 1 \{ P_2 \}$
\item $P_2 \Rightarrow P'$
\item $\{P' \land G\} \kw{While}... \{ P'\}$
\item $P' \land \neg G \Rightarrow \mathcal{O}$
\end{itemize}
\end{minipage}
\end{tabular}

\end{frame}

\section{Exercício}

\begin{frame}
\frametitle{Soma dos elementos de um arranjo}

\begin{codebox}
\Procname{$\proc{ArrayMax}(A)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I} : A = \langle a_1 \cdots a_n \rangle \land n > 0 \}$}
\li $r \gets 0$
\li $i \gets 1$
\li \While $i \le \id{Length}[A]$
\li \Do
\li   $r \gets r + A[i]$
\li   $i \gets i + 1$
    \End
\li \Return $r$
\zi \Comment \textcolor{blue}{$\{ \mathcal{O} : r = \sum_{i=1}^n a_i \}$}
\end{codebox}  

Desenvolva a prova que o algoritmo é correto.
\end{frame}

\end{document}