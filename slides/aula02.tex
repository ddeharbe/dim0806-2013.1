\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[brazil,british]{babel}
\usetheme{default} 
\usecolortheme{beaver}
\usepackage{graphicx}
\usepackage{clrscode}
\usepackage{hyperref}
% \usecolortheme{dove}
\title{Aula 02}
\author{David Déharbe \\
  Programa de Pós-graduação em Sistemas e Computação \\
  Universidade Federal do Rio Grande do Norte \\
  Centro de Ciências Exatas e da Terra \\
  Departamento de Informática e Matemáica Aplicada}
\date{22 de fevereiro de 2013}
\logo{\includegraphics[width=1cm]{img/logo-ppgsc-icon-text.png}}

\begin{document}
\selectlanguage{brazil}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Plano da aula}
  \tableofcontents
\end{frame}

\section{Lógica de Hoare}

\begin{frame}

\frametitle{Introdução à lógica de Hoare}

Uma fórmula da lógica de Hoare:
\begin{itemize}
\item é chamada \emph{tripla de Hoare};
\item tem como objetivo expressar o efeito de um comando sobre o
  estado do computador (as variáveis do programa);
\item tem a seguinte aparência:
\[
  \{P \} C \{Q\}
\]
onde
\item $P$ e $Q$ são asserções lógicas acerca das variáveis do programa;
\begin{itemize}
\item $P$ é a pré-condição, e
\item $Q$ é a pós-condição;
\end{itemize}
\item $C$ é um comando de computador.
\end{itemize}

\end{frame}

\begin{frame}

\frametitle{Introdução à lógica de Hoare}

A tripla $\{P \} C \{Q\}$ é válida se e somente se
\begin{itemize} 
\item cada vez que o estado do programa satisfaz a pré-condição $P$,
\item quando o comando $C$ é executado, 
\item o estado do programa passa a satisfazer a pós-condição $Q$.
\end{itemize}

\begin{block}{Exemplo de tripla válida:}
\[
\{ x = 3 \} x \gets x + 1 \{ x = 4 \}
\]
\end{block}

\end{frame}

\begin{frame}

\frametitle{Relação entre tripla de Hoare e correção de algoritmo?}

\begin{block}{Exemplo}
  Considere o problema da busca linear:
  \begin{align*}
    D & \equiv A, v \\
    \mathcal{I}(A, v) & \equiv A = \langle a_1, a_2, \ldots, a_n \rangle \\
    \mathcal{O}(A, v, r) & \equiv \begin{array}[t]{l}
      (A = \langle a_1, a_2, \ldots, a_n \rangle) \land \\
      ((1 \le r \le n \land a_r = v) \lor \\
      \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})
      \end{array}
  \end{align*}
\end{block}

Verificar o algoritmo \proc{Linear-Search} consiste em provar que a seguinte
tripla de Hoare é válida:
\[
\{ \mathcal{I}(A, v) \} \proc{Linear-Search}(A, v) \{ \mathcal{O}(A, v, r) \}
\]
\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

\[
\{ \mathcal{I}(A, v) \} \proc{Linear-Search}(A, v) \{ \mathcal{O}(A, v, r) \}
\]

  \begin{align*}
    \mathcal{I}(A, v) & \equiv A = \langle a_1, a_2, \ldots, a_n \rangle \\
    \mathcal{O}(A, v, r) & \equiv \begin{array}[t]{l}
      (A = \langle a_1, a_2, \ldots, a_n \rangle) \land \\
      ((1 \le r \le n \land a_r = v) \lor \\
      \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})
      \end{array}
  \end{align*}

\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\li $j \gets 1$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle\}$
\li $j \gets 1$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\zi \Comment $\{\begin{array}[t]{l}
  A = \langle a_1, a_2, \ldots, a_n \rangle \land \\
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}$
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

\begin{itemize}
  \item Construir a prova por decomposição do objetivo em sub-objetivos
    mais simples;
  \item Inserir asserções intermediárias;
  \item Provar os sub-objetivos.
\end{itemize}

\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle\}$
\li $j \gets 1$
\zi \Comment $\{ \textcolor{red}{\cdots} \}$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\zi \Comment $\{ \textcolor{red}{\cdots} \}$
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\zi \Comment $\{A = \langle a_1, a_2, \ldots, a_n \rangle \land \ldots \}$
\end{codebox}  
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

A lógica de Hoare possui regras que permitem realizar esta decomposição
\begin{itemize}
  \item atribuição $\gets$
  \item comando vácuo \kw{Skip}
  \item sequenciamento
  \item conditional
  \item laço
\end{itemize}

\pause
\begin{block}{Notação:}
$H_1, H_2, \ldots H_n \vdash C$ denota que se \emph{todas} as hipóteses
$H_i$ são verdadeiras, então a conclusão $C$ é verdadeira.
\end{block}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare: \kw{Skip}}

O comando vácuo faz nada:
\[
\vdash \{P\} \kw{Skip} \{P\}
\]

\pause 
\begin{block}{Observação:}
$\vdash C$ significa que $C$ é verdade independente de qualquer hipótese.
\end{block}

\pause
\begin{block}{Explicações:}
\begin{itemize}
\item se $P$ for verdade antes de executar $\kw{Skip}$ também será verdade
  depois;
\item se $P$ for verdade depois de executar $\kw{Skip}$ também será verdade
  antes.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Notação útil: substituição}

$P \lbrack E / x \rbrack$ é a fórmula $P$ onde $x$ foi substituído pela
expressão $E$.

\pause
\begin{block}{Exemplo}
\begin{align*}
& v = 42 \lbrack v+1 / v \rbrack \\
\leadsto & \underbrace{x = 42}_P \lbrack \underbrace{v+1}_E / \underbrace{v}_x \rbrack \\
\leadsto & v+1 = 42 \\
\iff & v = 41
\end{align*}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare: $\gets$}

Atribuição do valor de uma expressão $E$ a uma variável $x$:
\[
\vdash \{P \lbrack E / x \rbrack\} x \gets E \{P\}
\]

\pause
\begin{block}{Exemplo: Prove que $\{ x = 41 \} x \gets x+1 \{ x = 42\}$.}
Aplicando as regras da lógica de Hoare e da aritmética inteira:
\begin{align*}
  \vdash & \{ x = 42 \lbrack x+1 / x \rbrack\} x \gets x+1 \{ x = 42\} & \mbox{regra da atribuição} \\
  \vdash & \{ x+1 = 42 \} x \gets x+1 \{ x = 42\} & \mbox{substituição} \\
  \vdash & \{ x = 41 \} x \gets x+1 \{ x = 42\} & \mbox{aritmética}
\end{align*}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare: sequência}

Composição sequencia de dois comandos:
\[
\underbrace{\{P\} C_1 \{Q\}, \{Q\} C_2 \{R\}}_{\mbox{\footnotesize hipóteses}} \vdash \underbrace{\{P\} C_1 ; C_2 \{R\}}_{\mbox{\footnotesize conclusão}}
\]

\end{frame}

\begin{frame}
\frametitle{Exercícios}

\begin{itemize}
\item Prove que $\{ x = 40 \} x \gets x+1 ; x \gets x+1 \{ x = 42\}$.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Resolução}

Para provar $\{ x = 40 \} x \gets x+1 ; x \gets x+1 \{ x = 42\}$, temos
que usar a regra da sequência de comandos.
\[
\{P\} C_1 \{Q\}, \{Q\} C_2 \{R\} \vdash \{P\} C_1 ; C_2 \{R\}
\]

Então, temos que casar o nosso objetivo com a conclusão da regra:
\[
\{P\} C_1 \{Q\}, \{Q\} C_2 \{R\} \vdash \{\underbrace{P}_{x=40}\} \underbrace{C_1}_{x \gets x+1} ; \underbrace{C_2}_{x \gets x+1} \{\underbrace{R}_{x=42}\}
\]

Realizando as substituições nas hipóteses, temos agora que provar:
\begin{align*}
\{x=40\} x \gets x+1 \{Q\} & \mbox{ e}\\
\{Q\} x \gets x+1 \{x=42\}.
\end{align*}
Temos que achar uma fórmula $Q$ tal que, completando as duas triplas, obtemos
triplas válidas.

\end{frame}

\begin{frame}
\frametitle{Exercícios}

\begin{itemize}
\item Prove que $\{ x = a \} x \gets x + x \{ x = 2\times a \}$.
\item Prove que $\{ x > y \} z \gets x ; x \gets y; y \gets z \{ y > x\}$.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Resolução}

Achar $Q$ tal que as triplas seguintes sejam válidas:
\begin{align*}
\{x=40\} x \gets x+1 \{Q\} \\
\{Q\} x \gets x+1 \{x=42\}
\end{align*}
\begin{itemize}
\item A regra para a atribuição: $\vdash \{P \lbrack E / x \rbrack\} x \gets E \{P\}$
\item Casar a regra com uma das triplas (a segunda):
\[
\vdash \{\underbrace{P \lbrack E / x}_{Q} \rbrack\} \underbrace{x}_{x} \gets \underbrace{E}_{x+1} \{\underbrace{P}_{x=42}\}
\]
\item Logo $Q \equiv x=42 \lbrack x+1/x \rbrack \iff (x+1=42) \iff x = 41$.
\item $\{x=40\} x = x+1 \{x=41\}$ é válida (regra da atribuição). \qed
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Exercícios}

Como verificar uma relação entre o valor do resultado e o valor (inicial)
dos parâmetros?

\begin{itemize}
\item Prove que $\{ true \} x \gets x \times x; x \gets x \times x \{ ??? \}$.
\end{itemize}

Precisamos verificar uma relação entre os valores inicial e final da variável de
programa $x$.

A variável matemática $x_0$ denota o valor inicial da variável de programa $x$.

A variável matemática $x$ denota o valor atual da variável de programa $x$.

\[
\begin{array}{lrcl}
  & \{ true \} & x \gets x \times x; x \gets x \times x & \{ x = x_0^4 \} \\
  \leadsto & \{ x = x_0 \} & x \gets x \times x; x \gets x \times x & \{ x = x_0^4 \}
\end{array}
\]

Atenção: uma variável matemática representa um valor fixo enquanto que uma
variável de programa representa um valor mutável.
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Condicional}
\[
\{ B \land P \} C_1 \{Q\},
\{ \neg B \land P \} C_2 \{Q\} 
\quad \vdash \quad \{P\} \kw{If} \, B \, \kw{Then} \, C_1 \, \kw{Else} \, C_2 \, \{Q\}
\]
\end{block}
\pause

\begin{block}{Exercício}
Escreva uma regra para o \kw{If} sem \kw{Else}.
\end{block}

\begin{block}{Dica}
Use o comando \kw{Skip}.
\end{block}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Consequência:}
$P' \Rightarrow P, \{ P \} C \{Q\}, Q \Rightarrow Q' \quad \vdash \quad \{P'\} C \{Q'\}$.
\end{block}

\pause

\begin{block}{Exemplo}
$
\begin{array}{l}
x < y \Rightarrow x \le y, \\
\{ x \le y \} y \gets y + 1 \{ x < y \}, \\
x < y \Rightarrow x \le y \quad
\end{array}
 \vdash \quad
\{x \le y\}  y \gets y + 1 \{ x \le y \}.
$
\end{block}

\pause

\begin{lemma}[Fortalecimento da pré-condição]
$P' \Rightarrow P, \{ P \} C \{Q\} \quad \vdash \quad \{P'\} C \{Q\}$.
\end{lemma}

\pause
\begin{lemma}[Enfraquecimento da pós-condição]
$\{ P \} C \{Q\}, Q \Rightarrow Q' \quad \vdash \quad \{P\} C \{Q'\}$.
\end{lemma}

\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Laço}
$\{ P \land G\} C \{P\} \quad \vdash \quad \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$.
\end{block}

\pause

\begin{block}{Exemplo 1}
$
\begin{array}{l}
\{ x \le n \land y = a^x \} \\
\kw{While} \, x < n \, \kw{Do} \, x \gets x + 1; y \gets y \times a \\
\{ x = n \land y = a^n \}.
\end{array}
$

\pause

Primeiro reescrever a pós-condição em uma forma equivalente, e no formato da
regra do laço.

$\begin{array}{rcl}
x = n \land \neg \land y = a^n 
& \iff & x = n \land y = a^x \\
& \iff & x \ge n \land x \le n \land y = a^x \\
& \iff & \neg x < n \land x \le n \land y = a^x
\end{array}$

\end{block}

\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Laço}
$\{ P \land G\} C \{P\} \quad \vdash \quad \{P\} \kw{While}\, G \, \kw{Do} \, C \, \{ \neg G \land P\}$.
\end{block}

\begin{block}{Exemplo 1}
$
\begin{array}{l}
\{ \underbrace{x \le n \land y = a^x}_{P} \} \\
\kw{While} \, \underbrace{x < n}_{G} \, \kw{Do} \, \underbrace{x \gets x + 1; y \gets y \times a}_{C} \\
\{ \neg \underbrace{x < n}_G \land \underbrace{x \le n \land y = a^x}_{P} \}.
\end{array}
$
\pause

Pela regra, deve-se então provar:
$
\begin{array}{l}
\{ x \le n \land y = a^x \land x < n\} x \gets x + 1; y \gets y \times a
\{ x \le n \land y = a^x \} \\
\iff 
\{ x < n \land y = a^x \} x \gets x + 1; y \gets y \times a \{ x \le n \land y = a^x \}
\end{array}
$
\end{block}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Exemplo 1}
Deve-se provar:
$
\begin{array}{l}
\{ x < n \land y = a^x \} x \gets x + 1; y \gets y \times a \{ x \le n \land y = a^x \}
\end{array}
$
\end{block}
De quais regras vamos precisar?

\pause
\begin{itemize}
\item sequência
\item atribuição
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Exemplo 1}
Deve-se provar:
$
\begin{array}{l}
\{ x < n \land y = a^x \} x \gets x + 1; y \gets y \times a \{ x \le n \land y = a^x \}
\end{array}
$
\begin{itemize}
\item Pela regra da sequência, precisamos encontrar $Q$ tal que:
$
\begin{array}{l}
\{ x < n \land y = a^x \} x \gets x + 1 \{Q\} \\
\{Q\} y \gets y \times a \{ x \le n \land y = a^x \}
\end{array}
$
\item E pela regra da atribuição e a segunda tripla:
$Q \equiv x \le n \land y \times a = a^x$
\item A tripla $\{ x < n \land y = a^x \} x \gets x + 1 \{x \le n \land y \times a = a^x\}$ é válida pela regra da atribuição e propriedades da aritmética:
$x < n \land y = a^x \iff x+1 \le n \land y \times a = a ^{x+1}$.
\end{itemize}
\end{block}

\end{frame}

% \begin{frame}

% \begin{block}{Exemplo 2}

% $
% \begin{array}{l}
% \{ x \le n \land y = a^x \} \\
% \kw{While} \, x < n \, \kw{Do} \, y \gets y \times a \\
% \{ x = n \land y = a^n \}.
% \end{array}
% $

% \end{block}

% \pause

% Esta regra é uma \emph{correção parcial}: não garante o fim do cálculo.

% \end{frame}

\begin{frame}
\frametitle{Regras da lógica de Hoare}

\begin{block}{Laço: correção total}
$\begin{array}{l}
\prec \mbox{ é uma ordem bem fundada}, 
\lbrack P \land G \land t = z \rbrack C \lbrack P \land t \prec z \rbrack \\
\quad \vdash \quad \lbrack P \rbrack \kw{While}\, G \, \kw{Do} \, C \lbrack \neg G \land P \rbrack.
\end{array}$
\end{block}
$t$ é o variante do laço. 

A execução do corpo do laço deve diminuir seu valor.

Como a ordem é bem fundada, não existe sequências infinitamente decrescente de
valores, e o laço termina.


\begin{block}{Exemplo}
$
\begin{array}{l}
\{ x \le n \land y = a^x \land t = n - x\} \\
\kw{While} \quad x < n \quad \kw{Do} \quad x \gets x + 1; y \gets y \times a \quad \\
\{ x = n \land y = a^n \}.
\end{array}
$
\end{block}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

\[
\{ \mathcal{I}(A, v) \} \proc{Linear-Search}(A, v) \{ \mathcal{O}(A, v, r) \}
\]

  \begin{align*}
    \mathcal{I}(A, v) & \equiv A = \langle a_1, a_2, \ldots, a_n \rangle \\
    \mathcal{O}(A, v, r) & \equiv \begin{array}[t]{l}
      A = \langle a_1, a_2, \ldots, a_n \rangle \land \\
      ((1 \le r \le n \land a_r = v) \lor \\
      \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})
      \end{array}
  \end{align*}

\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\li $j \gets 1$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \}$
\li $j \gets 1$
\li \While $A[j] \neq v$ and $j \le \id{length}(A)$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le \id{length}(A)$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\zi \Comment $\{\begin{array}[t]{l}
  A = \langle a_1, a_2, \ldots, a_n \rangle \land \\
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}$
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

$\id{length}(A) = n$, $A[j] = a_j$ e simplificamos para:
\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \}$
\li $j \gets 1$
\li \While $a_j \neq v$ and $j \le n$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le n$
\li \Then
      \Return $j$
\li \Else
      \Return \const{nil}
    \End
\zi \Comment $\{\begin{array}[t]{l}
  A = \langle a_1, a_2, \ldots, a_n \rangle \land \\
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}$
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

Explicitamos a atribuição do resultado $r$:
\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \}$
\li $j \gets 1$
\li \While $a_j \neq v$ and $j \le n$
\li \Do
      $j \gets j+1$
    \End
\li \If $j \le n$
\li \Then
      $r \gets j$
\li \Else
      $r \gets \const{nil}$
    \End
\zi \Comment $\{\begin{array}[t]{l}
  A = \langle a_1, a_2, \ldots, a_n \rangle \land \\
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}$
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

O algoritmo é uma sequência de 4 comandos, e tentamos adivinhar asserções
intermediárias que vão compor a prova:
\begin{codebox}
\Procname{$\proc{Linear-Search}(A, v)$}
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \}$
\li $j \gets 1$
\zi \Comment $\{ A = \langle a_1, a_2, \ldots, a_n \rangle \land j = 1\}$
\li \While $a_j \neq v$ and $j \le n$
\li \Do
      $j \gets j+1$
    \End
\zi \Comment $\{A = \langle a_1, a_2, \ldots, a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)\}
  \end{array}$
\li \If $j \le n$
\li \Then
      $r \gets j$
\li \Else
      $r \gets \const{nil}$
    \End
\zi \Comment $
  \{A = \langle a_1, a_2, \ldots, a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}$
\end{codebox}  

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

Temos agora decomposto a prova em três:

\begin{enumerate}
\item Inicialização:
$\begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \} \\
j \gets 1 \\
\{ A = \langle a_1 \cdots a_n \rangle \land j = 1\}
\end{array}$
\item Laço:
$\begin{array}{l}
\{ A = \langle a_1 \cdots a_n \rangle \land j = 1\} \\
\kw{While}\, a_j \neq v \mbox{ and } j \le n \, \kw{Do} \, j \gets j+1 \\
\{A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)\}
  \end{array}
\end{array}$
\item Retorno:
$\begin{array}{l}
\{A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)\}
  \end{array} \\
\kw{If}\, j \le n \,\kw{Then}\, r \gets j \,\kw{Else}\, r \gets \const{nil} \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\end{enumerate}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: inicialização}

Sub-objetivo 1:
$\begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \} \\
j \gets 1 \\
\{ A = \langle a_1 \cdots a_n \rangle \land j = 1\}
\end{array}$

\begin{itemize}
\item Usamos a regra:
$
\vdash \{P \lbrack E / x \rbrack\} x \gets E \{P\}$
 com $x : j$, $E : 1$ e $P : A = \langle a_1 \cdots a_n \rangle \land j = 1$.
\item Ou seja:
$\vdash \begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \land 1 = 1\} \\
j \gets 1 \\
\{ A = \langle a_1 \cdots a_n \rangle \land j = 1\}
\end{array}$
\item Mas:
$\begin{array}[t]{l}
A = \langle a_1 \cdots a_n \rangle \land 1 = 1 \iff \\
A = \langle a_1 \cdots a_n \rangle \land true \iff \\
A = \langle a_1 \cdots a_n \rangle.
\end{array}$
\item Logo está provado o primeiro sub-objetivo.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: retorno}

Sub-objetivo 3:
$\begin{array}[t]{l}
\{A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)\}
  \end{array} \\
\kw{If}\, j \le n \,\kw{Then}\, r \gets j \,\kw{Else}\, r \gets \const{nil} \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$

\begin{itemize}
\item Usamos a regra:
$\{ B \land P \} C_1 \{Q\},
\{ \neg B \land P \} C_2 \{Q\} 
\vdash \{P\} \kw{If} \, B \, \kw{Then} \, C_1 \, \kw{Else} \, C_2 \, \{Q\}$
\item $B : j \le n$, $C_1 : r \gets j$, $C_2 : r \gets \const{nil}$
\item $P : A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)
  \end{array}$
\item $Q : \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})
  \end{array}$
\end{itemize}
E obtemos dois sub-objetivos...
\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: sub-objetivo 3.1}

Sub-objetivo 3.1:
$\begin{array}[t]{l}
  \{ j \le n \land A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
    (1 \le j \le n \land a_j = v \lor \\
    \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v) \}
  \end{array} \\
  r \gets j \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$

\pause
\begin{itemize}
\item Simplificamos a pré-condição, usando lógica Booleana:
$\begin{array}[t]{l}
  \{ A = \langle a_1 \cdots a_n \rangle \land
    1 \le j \le n \land a_j = v \} \\
  r \gets j \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: 3.1}

$\begin{array}[t]{l}
  \{ A = \langle a_1 \cdots a_n \rangle \land
    1 \le j \le n \land a_j = v \} \\
  r \gets j \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$

\pause
\begin{itemize}
\item Aplicando a lei para a atribuição, temos:

$\vdash \begin{array}[t]{l}
  \{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land j = \const{nil})\}
  \end{array}\} \\
  r \gets j \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$

\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: 3.1}

\begin{itemize}
\item Lei do fortalecimento da pré-condição:
$P' \Rightarrow P, \{ P \} C \{Q\} \quad \vdash \quad \{P'\} C \{Q\}$.
\item Leis da lógica ($P \Rightarrow P \lor Q$):
$\begin{array}[t]{l}
A = \langle a_1 \cdots a_n \rangle \land 1 \le j \le n \land a_j = v \\
\Rightarrow
A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land j = \const{nil})
\end{array}
\end{array}
$
\item Conclusão:
$\begin{array}[t]{l}
  \{ A = \langle a_1 \cdots a_n \rangle \land
    1 \le j \le n \land a_j = v \} \\
  r \gets j \\
  \{A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\end{itemize}
O sub-objetivo 3.1 foi demostrado.
\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: sub-objetivo 3.2}

Sub-objetivo 3.2:
$\begin{array}[t]{l}
\{ \neg j \le n \land A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v) \}
  \end{array} \\
r \gets \const{nil} \\
\{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$

\pause
\begin{itemize}
\item Simplificamos a pré-condição, usando lógica Booleana e aritmética inteira:
$\begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \land
   j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v) \} \\
r \gets \const{nil} \\
\{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: sub-objetivo 3.2}

$\begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \land
   j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v) \} \\
r \gets \const{nil} \\
\{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\pause
\begin{itemize}
\item Aplicando a lei para a atribuição, temos:

$\vdash \begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le \const{nil} \le n \land a_\const{nil} = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land 
  \end{array}\\
r \gets \const{nil} \\
\{ A = \langle a_1 \cdots a_n \rangle \land \begin{array}[t]{l}
  (1 \le r \le n \land a_r = v \lor \\
  \quad \forall i : 1 \le i \le n \Rightarrow a_i \neq v \land r = \const{nil})\}
  \end{array}
\end{array}$
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

To be continued...

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}: laço}

Sub-objetivo 2:
$\begin{array}[t]{l}
\{ A = \langle a_1 \cdots a_n \rangle \land j = 1\} \\
\kw{While}\, a_j \neq v \mbox{ and } j \le n \, \kw{Do} \, j \gets j+1 \\
\{A = \langle a_1 \cdots a_n \rangle \land
  \begin{array}[t]{l}
  (1 \le j \le n \land a_j = v \lor \\
  \quad j > n \land \forall i : 1 \le i \le n \Rightarrow a_i \neq v)\}
  \end{array}
\end{array}$

\end{frame}

\begin{frame}
\frametitle{Correção do algoritmo \proc{Linear-Search}}

To be continued...

\end{frame}

\begin{frame}

  \frametitle{Problema}

  \begin{itemize}
  \item Especifique o problema da $n$-ésima potência de um número.

  \item Escreva um algoritmo que calcula a $n$-ésima potência de um número $a$,
    sendo $n \ge 0$, supondo disponíveis as operações aritméticas de soma e
    multiplicação.

  \item Prove seu algoritmo correto.
  \end{itemize}

\end{frame}



\end{document}

\begin{frame}

\frametitle{Relação entre tripla de Hoare e correção de algoritmo?}

Supondo um problema computacional tal que 
\begin{itemize}
\item as instâncias do problema são descritas com a asserção $\mathcal{I}(D_0)$,
  onde $D_0$ são os dados iniciais do problema
\item o problema é especificado como $\mathcal{O}(D_0, D, r)$, onde $D$ são os
  dados finais do problema, e $r$ qualquer resultado adicional possível.
\end{itemize}
Um algoritmo correto $A(D, r)$ para este problema é tal que:
\[
\{ \mathcal{I}(D) \} A(D, r) \{ \mathcal{O}(D_0, D, r) \}.
\]
Assumimos que, para o algoritmo, $D$ são parâmetros passados por referência e
$r$ é o resultado.
\end{frame}

