\documentclass[handout]{beamer}
\setbeamertemplate{footline}[frame number]
%\documentclass{beamer}

\input{preamble}

\usepackage{pgf}
\usepackage{tikz}

\title{Aula 08: Análise matemática de algoritmos recursivos}
\author{David Déharbe \\
  Programa de Pós-graduação em Sistemas e Computação \\
  Universidade Federal do Rio Grande do Norte \\
  Centro de Ciências Exatas e da Terra \\
  Departamento de Informática e Matemáica Aplicada}
\date{15 de março de 2013}

\begin{document}
\selectlanguage{brazil}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Plano da aula}
  \tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}

  \frametitle{Contexto}

  \begin{center}
  \input{fig/process}
  \end{center}
\end{frame}


\begin{frame}

  \frametitle{Estrutura da apresentação}

  \begin{description}
  \item[08/03 pt.1] arcabouço de análise, noção de crescimento assintótico;
  \item[08/03 pt.2] notações assintóticas; $O$, $\Omega$, $\Theta$;
  \item[15/03 pt.1] análise de algoritmos não recursivos;
  \item[15/03 pt.2] \alert{análise de algoritmos recursivos}.
  \end{description}
\end{frame}

\begin{frame}

  \frametitle{Bibliografia usada}

  \begin{center}
    \includegraphics[height=.8\textheight]{img/capa-levitin.jpg}
  \end{center}
  (seções 2.4, 2.5)
\end{frame}

\section{Um exemplo introdutório}

\begin{frame}
  \frametitle{Exemplo introdutório}

  \begin{example}[Fatorial]
\begin{codebox}
\Procname{$\proc{F}(n)$}
\li \If $n = 0$ \kw{then} \Return $1$
\li \Else \Return $n \times F(n-1)$
\end{codebox}
  \end{example}
  \pause

\begin{itemize}
\item Tamanho da entrada: $n$.
\item Operação básica: \alert{multiplicação}/teste $n=0$/chamada recursiva
\item Seja $M(n)$ o número de multiplicações:
\begin{itemize}
\item $M(n) = 1 + M(n-1)$
\item $M(0) = 0$
\end{itemize}
\item $M$ é definida por recorrência. Como encontrar uma expressão fechada,
não recursiva, de $M(n)$?
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Resolução de recorrência por substituição}

\begin{eqnarray*}
M(n) & = & 1 + M(n-1) \\
& = & 1 + (1 + M(n-2)) = 2 + M(n-2) \\
& = & 2 + (1 + M(n-3)) = 3 + M(n-3) 
\end{eqnarray*}
\begin{itemize}
\item Conjectura: $M(n) = i + M(n-i)$
\item Como é conhecido $M(0) = 0 = M(n-n)$, então $M(n) = n + M(0) = n$.
\end{itemize}
\end{frame}

\section{Estratégia de análise}

\begin{frame}
\frametitle{Estratégia de análise}
\begin{enumerate}
\item Identificar um parâmetro representando o tamanho da entrada
\item Identificar a operação básica do algoritmo
\item Verificar se o número de vezes que a operação básica é executada pode
  variar com entradas do mesmo tamanho.

  Se for o caso, o pior caso, a complexidade média e o melhor caso devem ser
  averiguados individualmente.
\item Estabelecer uma relação de recorrência que corresponde ao número de vezes que uma operação é executada. Estabelecer o valor inicial também.

\item Resolver a recorrência. Pelo menos, enquadrar o crescimento asintótico da
  relação.
\end{enumerate}
\end{frame}

\section{Exemplo 2}
\begin{frame}
\frametitle{Torres de Hanoi}

\includegraphics{img/hanoi.jpeg}

A Torre de Hanói é um "quebra-cabeça" que consiste em uma base contendo três
pinos, em um dos quais são dispostos alguns discos uns sobre os outros, em ordem
crescente de diâmetro, de cima para baixo. O problema consiste em passar todos
os discos de um pino para outro qualquer, usando um dos pinos como auxiliar, de
maneira que um disco maior nunca fique em cima de outro menor em nenhuma
situação.  (Wikipedia)

\end{frame}

\begin{frame}
\frametitle{Resolução (recursiva)}

\begin{codebox}
\Procname{$\proc{Hanoi}(N, \id{src}, \id{dest}, \id{aux})$} 
\li \If $n = 1$
\li \Then
\li \Return $\proc{Move}(\id{src}, \id{dest})$
\li \Else
\li   $\proc{Hanoi}(N-1, \id{src}, \id{aux}, \id{dest})$
\li   $\proc{Move}(\id{src}, \id{dest})$
\li   $\proc{Hanoi}(N-1, \id{aux}, \id{dest}, \id{src})$
    \End
\end{codebox}
\end{frame}

\begin{frame}
\frametitle{Aplicação da estratégia}

\begin{small}
\begin{codebox}
\Procname{$\proc{Hanoi}(N, \id{src}, \id{dest}, \id{aux})$} 
\li \If $n = 1$
\li \Then
\li \Return $\proc{Move}(\id{src}, \id{dest})$
\li \Else
\li   $\proc{Hanoi}(N-1, \id{src}, \id{aux}, \id{dest})$
\li   $\proc{Move}(\id{src}, \id{dest})$
\li   $\proc{Hanoi}(N-1, \id{aux}, \id{dest}, \id{src})$
    \End
\end{codebox}
\end{small}
\begin{itemize}
\item Tamanho da entrada: $n$
\item Operação básica: mover um disco
\item $M(n) = M(n-1) + 1 + M(n-1) = 2 \times M(n-1) + 1$ se $n > 1$
\item $M(1) = 1$.
\item Resolver $M$.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Aplicação da estratégia}

\begin{eqnarray*}
M(n) & = & 2 \times M(n-1) + 1 \pause \\
     & = & 2 \times (2 \times M(n-2) + 1) + 1 \pause = 4 \times M(n-2) + 3 \pause \\
     & = & 4 \times (2 \times M(n-3) + 1) + 3 \pause = 8 \times M(n-3) + 7 \pause \\
     & \cdots & \\
     & = & 2^i \times M(n-i) + (2^i - 1) \pause \\
     & \cdots & \mbox{(note que $1 = n - (n - 1)$)}\\
     & = & 2^{n-1} \times M(n-(n-1)) + (2^{n-1} - 1) \pause \\
     & = & 2^{n-1} \times M(1) + 2^{n-1} - 1 \pause \\
     & = & 2^{n-1} + 2^{n-1} - 1 \pause \\
     & = & 2 \times 2^{n-1} - 1 \pause \\
\alert{M(n)} & \alert{=} & \alert{2^n - 1}
\end{eqnarray*}

\end{frame}

\begin{frame}
\frametitle{Observações}
\begin{itemize}
\item A resolução recursiva do problema das Torres de Hanoi cabe em 5 linhas.
\item Tem complexidade exponencial
\item No caso, não tem solução mais eficiente.
\item Mas cuidado: concisão não é sinônimo de eficiência!
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Árvore das chamadas recursivas}
Permite visualizar todas as chamadas realizadas por um algoritmo recursivo: pode ser útil para analizar a complexidade.

\begin{center}
\input{fig/hanoi-calltree}
\end{center}

\end{frame}


\section{Exemplo 3}
\begin{frame}
\frametitle{Número de algarismos binários de um inteiro}

\begin{codebox}
\Procname{$\proc{Binary}(n$)} 
\li \If $n \le 1$
\li \Then
\li   \Return $1$
\li \Else
\li   \Return $\proc{Binary}(\lfloor n/2 \rfloor)$ + 1
    \End
\end{codebox}

\end{frame}

\begin{frame}
\frametitle{Aplicação da estratégia}
\begin{small}
\begin{codebox}
\Procname{$\proc{Binary}(n$)} 
\li \If $n \le 1$
\li \Then
\li   \Return $1$
\li \Else
\li   \Return $\proc{Binary}(\lfloor n/2 \rfloor)$ + 1
    \End
\end{codebox}
\end{small}
\begin{itemize}
\item Tamanho da entrada: $n$
\item Operação básica: adição
\item $A(n) = 1+A(\lfloor n/2 \rfloor)$
\item $A(1) = A(0) = 0$.
\item Resolver $A$.
\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item $A(n) = 1+A(\lfloor n/2 \rfloor)$
\item $A(1) = A(0) = 0$.
\item Resolver $A$?
\end{itemize}
Vamos considerar o caso de $n = 2^k$ ($n$ é potência de 2).
\begin{eqnarray*}
A(n) & = & 1 + A (\lfloor 2^k/2 \rfloor) \pause = 1 + A (2^{k-1}) \pause \\
     & = & 1 + (1 + A(\lfloor 2^{k-1}/2 \rfloor) \pause = 2 + A(2^{k-2}) \pause \\
     & \cdots & \\
     & = & i + A(2^{k-i}) \pause \\
     & \cdots & \\
     & = & k + A(2^{k-k}) \pause = k + A(2^0) \pause = k + A(1) \pause \\
     & = & k \pause \\
     & = & \log_2 n \in \Theta(\log n) 
\end{eqnarray*}
\end{frame}

\section{Interlúdio calculatório}
\begin{frame}
\frametitle{Resolução de relações de recorrência}

Recorrências lineares de segunda ordem com coeficientes constantes:
$$a \times x(n) + b \times x(n-1) + c \times x(n-2) = f(n).$$

Caso $f(n) = 0$, então a recorrência é \emph{homogênea}.

\pause

A equação $ax^2 + bx + c = 0$ é a \emph{equação característica} da recorrência.
\end{frame}

\begin{frame}
\frametitle{Resolução de relações de recorrência}

\begin{theorem}
Seja $r_1, r_2$ as raizes da equação característica de uma recorrência linear homogênea de segunda ordem com coeficientes constantes.
\begin{enumerate}
\item[caso 1] Se $r_1$ and $r_2$ são números reais e distintos, a solução geral é $x(n) = \alpha r_1^n + \beta r_2^n$.
\item[caso 2] Se $r_1 = r_2 = r$, uma solução geral é $x(n) = \alpha r^n + \beta  n r^n$.
\item[caso 3] Se $r_{1,2} = u \pm iv$ são complexos distintos, a solução geral é $x(n) = \gamma^n(\alpha \cos n \theta + \beta \sin n \theta)$, onde $\theta = \arctan v/u$, $\gamma = \sqrt{u^2 + v^2}$.
\end{enumerate}
Em todos os casos $\alpha$ e $\beta$ são constantes reais.
\end{theorem}

\end{frame}

\section{Exemplo 4: A sequência de Fibonacci}

\begin{frame}
\frametitle{Sequência de Fibonacci}

Sequência definida pelo matemático Fibonacci:
$$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, \ldots$$

A sequência é definida por recorrência:
\begin{itemize}
\item caso geral: $F(n) = F(n-1) + F(n-2)$, se $n > 1$, 
\item condições iniciais: $F(0) = 0$ e $F(1) = 1$.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Sequência de Fibonacci}

\begin{itemize}
\item Note que: $F(n) - F(n-1) - F(n-2) = 0$, se $n > 1$. 

\item É uma relação de recorrência linear homogênea de segunda ordem, com
  coeficientes constantes.

\item Sua equação característica é $x^2 - x - 1 = 0$.

\item As raizes são $r_1 = \frac{1+\sqrt{5}}{2}$ e $r_2 = \frac{1-\sqrt{5}}{2}$.

\item O termo geral é $F(n) = \alpha(\frac{1+\sqrt{5}}{2})^n + \beta(\frac{1-\sqrt{5}}{2})^n$.

\item Com as condições iniciais, temos $\alpha + \beta = 0$ e $\frac{1+\sqrt{5}}{2}\alpha + \frac{1-\sqrt{5}}{2}\beta = 1$.

\item $F(n) = \frac{1}{\sqrt{5}}(\frac{1+\sqrt{5}}{2})^n - \frac{1}{\sqrt{5}}(\frac{1+\sqrt{5}}{2})^n = \frac{1}{\sqrt{5}}(\phi^n - \hat{\phi}^n) \in \Theta(\phi^n)$.
\end{itemize}
$\phi \approx 1,61803$ e $\hat{\phi} \approx - 0,61803$
\end{frame}

\begin{frame}
\frametitle{Sequência de Fibonacci}

\begin{example}[Fibonacci]
\begin{codebox}
\Procname{$\proc{Fib}(n)$}
\li \If $n \le 1$ \kw{then} \Return $n$
\li \Else \Return $\proc{Fib}(n-1) + \proc{Fib}(n-2)$
\end{codebox}
\end{example}
\pause
\begin{itemize}
\item tamanho da entrada: $n$
\item operação básica: adição
\item custo:
\begin{itemize}
\item caso geral: $A(n) = A(n-1) + A(n-2) + 1$
\item condições gerais: $A(0) = A(1) = 0$.
\end{itemize}
\item resolver $A(n)$...
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Sequência de Fibonacci}

\begin{itemize}
\item Resolver:
\begin{itemize}
\item caso geral: $A(n) = A(n-1) + A(n-2) + 1$
\item condições gerais: $A(0) = A(1) = 0$.
\end{itemize}
\item temos: $A(n) - A(n-1) - A(n-2) = 1$ não é homogênea.
\item seja $B(n) = A(n)+1$: $B(n) - B(n-1) - B(n-2) = 0$ é homogênea!
\item mais ainda: $B(n) = F(n+1)$, logo $B(n) = \frac{1}{\sqrt{5}}(\phi^{n+1} - \hat{\phi}^{n+1}) \in \Theta(\phi^n)$.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Sequência de Fibonacci: versão eficiente}

\begin{itemize}
\item Sem recursividade
\begin{codebox}
\Procname{$\proc{Fib-NR}(n)$}
\li $F[0] \gets 0, F[1] \gets 1$
\li \For $i \gets 2$ \To $n$ \Do
\li   $F[i] \gets F[i-1] + F[i-2]$
    \End
\li \Return $F[n]$
\end{codebox}
\item Com recursividade
\begin{codebox}
\Procname{$\proc{Fib-Acc}(n, k, x, y)$}
\li \If $n = k$ \kw{then} \Return $x+y$
\li \Else \Return $\proc{Fib-Acc}(n, k+1, y, x+y)$
\end{codebox}
\begin{codebox}
\Procname{$\proc{Fib-R}(n)$}
\li \If $n \le 1$ \kw{then} \Return $n$
\li \Else \Return $\proc{Fib-Acc}(n, 2, 0, 1)$
\end{codebox}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Exercício}

Estabeleça a complexidade de $\proc{Fib-NR}$ e de $\proc{Fib-R}$.

\end{frame}

\end{document}