\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[brazil,british]{babel}

\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{clrscode}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{a4wide}

\title{Resolução de exercícios}
\author{David Déharbe \\
  Programa de Pós-graduação em Sistemas e Computação \\
  Universidade Federal do Rio Grande do Norte \\
  Centro de Ciências Exatas e da Terra \\
  Departamento de Informática e Matemáica Aplicada}

\newcommand{\True}[0]{\mbox{\sc True}}
\newcommand{\assert}[1]{\textcolor{blue}{\{#1\}}}

\begin{document}
\selectlanguage{brazil}
\maketitle

\section{Enunciado do exercício}

\begin{codebox}
\Procname{$\proc{ArraySum}(A)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I} : A = \langle a_1 \cdots a_n \rangle \land n > 0 \}$}
\li $r \gets 0$
\li $i \gets 1$
\li \While $i \le n$
\li \Do
\li   $r \gets r + a_i$
\li   $i \gets i + 1$
    \End
\li \Return $r$
\zi \Comment \textcolor{blue}{$\{ \mathcal{O} : r = \sum_{i=1}^n a_i \}$}
\end{codebox}  

Desenvolva a prova que o algoritmo é correto.

\section{Resolução}

Para mostrar o algoritmo correto, são duas etapas:
\begin{enumerate}
\item Elaborar asserções intermediárias;
\item Provar que as asserções são corretas.
\end{enumerate}
Provar que as asserções são corretas consiste em verificar que são válidas as
triplas de Hoare formadas com os comandos e as asserções correspondentes. Isto é
feito usando as regras da lógica de Hoare, da lógica proposicional, da
aritmética, etc.

No caso de comandos de repetição, verificar essas triplas de Hoare também
necessita encontrar o invariante da repetição.
\subsection{Asserções intermediárias}

O algoritmo é uma sequência de três instruções: duas atribuições e uma
repetição.  Logo são dois pontos intermediários e duas asserções intermediárias
$\mathcal{A}_1$ e $\mathcal{A}_2$, conforme ilustrado no algoritmo abaixo:

\begin{codebox}
\Procname{$\proc{ArraySum}(A)$}
\zi \Comment \textcolor{blue}{$\{ \mathcal{I} : A = \langle a_1 \cdots a_n \rangle \land n > 0 \}$}
\li $r \gets 0$
\zi \Comment \textcolor{blue}{$\{ \mathcal{A}_1 \}$}
\li $i \gets 1$
\zi \Comment \textcolor{blue}{$\{ \mathcal{A}_2 \}$}
\li \While $i \le n$
\li \Do
\li   $r \gets r + a_i$
\li   $i \gets i + 1$
    \End
\zi \Comment \textcolor{blue}{$\{ \mathcal{O} : r = \sum_{k=1}^n a_k \}$}
\li \Return $r$
\end{codebox}

Como as duas primeiras instruções são atribuições a variáveis locais, não tem
dificuldade em encontrar essas asserções:
\begin{eqnarray*}
\mathcal{A}_1 & = & A = \langle a_1 \cdots a_n \rangle \land n > 0 \land r = 0, \\
\mathcal{A}_2 & = & A = \langle a_1 \cdots a_n \rangle \land n > 0 \land r = 0 \land i = 1.
\end{eqnarray*}
A resolução do exercício pode ser realizada com essas duas asserções. Mas
podemos observar que $A$ não é referenciado no corpo do algoritmo e eliminar das
asserções a definição correspondente:
\begin{eqnarray*}
\mathcal{A}_1 & = & n > 0 \land r = 0, \\
\mathcal{A}_2 & = & n > 0 \land r = 0 \land i = 1.
\end{eqnarray*}
A resolução do exercício prosseguirá com a versão mais enxuta das asserções.

\subsection{Verificação das asserções}

Aplicando a regra do sequenciamento, para provar o algoritmo correto, devemos
provar válidas as seguintes triplas de Hoare:
\begin{enumerate}
\item Tripla I: 
$\begin{array}[t]{l}
\assert{A = \langle a_1 \cdots a_n \rangle \land n > 0} \\
\quad r \gets 0 \\
\assert{n > 0 \land r = 0}
\end{array}$
\item Tripla II: 
$\begin{array}[t]{l}
\assert{n > 0 \land r = 0} \\
\quad i \gets 1 \\
\assert{n > 0 \land r = 0 \land i = 1}
\end{array}$
\item Tripla III:
$\begin{array}[t]{l}
\assert{n > 0 \land r = 0 \land i = 1} \\
\quad \kw{While~} i \le n \kw{~Do~} r \gets r + a_i; i \gets i + 1 \\
\assert{r = \sum_{k=1}^n a_k}$$
\end{array}$
\end{enumerate}

\subsubsection{Verificação da tripla de Hoare I}

A seguinte derivação detalha precisamente como provar que a tripla de Hoare I é válida:
\begin{center}
\infer[^3]{\assert{A = \langle a_1 \cdots a_n \rangle \land n > 0} r \gets 0 \assert{n > 0 \land r = 0}}
{\infer[^2]{\assert{n > 0} r \gets 0 \assert{n > 0 \land r = 0}}{\infer[^1]{\assert{(n > 0 \land r = 0) \lbrack 0/r \rbrack} r \gets 0 \assert{n > 0 \land r = 0}}{} & \begin{array}[b]{l}
(n > 0 \land r = 0) \lbrack 0/r \rbrack \\
\Leftrightarrow n > 0 
\end{array}}& \begin{array}[b]{l}\left(A = \langle a_1 \cdots a_n \rangle \land n > 0\right) \\
\Rightarrow n > 0
\end{array}}
\end{center}

Explicamos a seguir cada uma das regras:
\begin{enumerate}
\item É uma instância da regra da atribuição: $\{ P\lbrack E/v \rbrack \} v \gets E \{ P \}$.
\item Simplifica a tripla anterior, usando a definição da substituição e as
  propriedades da igualdade ($x = x$ é $\True$) e da lógica proposicional ($x
  \land \True$ é $x$).
\item Aplica a regra da dedução (fortalecimento da pré-condição) à tripla
  resultante da etapa 2, usando uma implicação trivialmente válida ($P \land Q
  \Rightarrow P$ é uma tautologia proposicional).
\end{enumerate}

\subsubsection{Verificação da tripla de Hoare II}

A prova de validade da tripla de Hoare II também é uma aplicação direta e
trivial da regra da atribuição.

\infer[^2]{\assert{n > 0 \land r = 0} i \gets 1 \assert{n > 0 \land r = 0 \land i = 1}}
{\infer[^1]{\assert{(n > 0 \land r = 0 \land i = 1)[1/i]} i \gets 1 \assert{n > 0 \land r = 0 \land i = 1}}{} & \begin{array}[b]{l} (n > 0 \land r = 0 \land i = 1)[1/i] \\
\Leftrightarrow (n > 0 \land r = 0)
\end{array}}

\subsubsection{Verificação da tripla de Hoare III}

Após estas duas verificações bem elementares, vamos abordar a verificação de uma
tripla de Hoare que envolve uma construção de repetição. Teremos necessariamente
que usar a regra do laço para as triplas de Hoare:
\begin{center}
\infer{\assert{P} \kw{While~} G \kw{~Do~} C \assert{P \land \neg G}}{\assert{P \land G} C \assert{P}}
\end{center}
Nela $P$ é uma asserção, chamada de invariante do laço. 

No desenvolvimento da nossa prova, o laço está cercado por duas asserções:
$\mathcal{A}_2$ é estabelecida antes da execução do laço, e $\mathcal{O}$ tem
que ser estabelecida depois desta execução. Em resumo, para provar que a tripla
III é válida, precisamos achar uma asserção $\mathsf{P}$ tal que
\begin{enumerate}
\item $\mathsf{P}$ é um invariante do laço;
\item $\mathsf{P}$ é estabelecida antes do laço se executar, ou seja
  $\mathcal{A}_2$ implica $\mathsf{P}$;
\item após o laço terminar a sua execução, a pós-condição $\mathcal{O}$ é
  estabelecida, ou seja é $\mathsf{P} \land \neg i \le n$ implica $\mathcal{O}$;
\end{enumerate}

Ou seja $\mathsf{P}$ deve ser tal que possamos construir uma prova que terá a
seguinte estrutura:

\begin{center}
\infer[^3]{\assert{n > 0 \land r = 0 \land i = 1} \kw{While~} i \le n \kw{~Do~} r \gets r + a_i; i \gets i + 1 \assert{r = \sum_{k=1}^n a_k}}
{
\infer[^2]{{\assert{n > 0 \land r = 0 \land i = 1} \kw{While~} i \le n \kw{~Do~} r \gets r + a_i; i \gets i + 1 \assert{\mathsf{P} \land \neg i \le n}}}{\begin{array}[b]{l} n > 0 \land r = 0 \land i = 1 \\ \Rightarrow \mathsf{P}? \end{array} & \infer[^1]{\assert{\mathsf{P}} \kw{While~} i \le n \kw{~Do~} r + a_i; i \gets i + 1 \assert{\mathsf{P} \land \neg i \le n}}{\infer{\assert{\mathsf{P} \land i \le n} r \gets r + a_i; i \gets i + 1 \assert{\mathsf{P}}}{\cdots ? \cdots}}}
& \begin{array}[b]{l}\mathsf{P} \land \neg i \le n \\\Rightarrow r = \sum_{k=1}^n a_k?\end{array}}
\end{center}

Na sequência, iremos identificar tal invariante $\mathsf{P}$ e provar que ele satisfaz as três condições
necessárias para terminar de construir a prova.

\paragraph{Identificação do invariante $\mathsf{P}$:} Uma estratégia para
encontrar o invariante é simular manualmente e simbolicamente a execução do laço
e tentar identificar um padrão de fórmula relacionando as variáveis do
algoritmo.
$$
\begin{array}{l|r|r}
                & i & r \\
\hline
\hline
                & 1 & 0 \\
\hline
r \gets r + a_i & 1 & a_1 \\
i \gets i + 1   & 2 & a_1 \\
\hline
r \gets r + a_i & 2 & a_1+a_2 \\
i \gets i + 1   & 3 & a_1+a_2 \\
\hline
r \gets r + a_i & 3 & a_1+a_2+a_3 \\
i \gets i + 1   & 4 & a_1+a_2+a_3 \\
\hline
\multicolumn{3}{c}{\cdots} \\
\hline
r \gets r + a_i & k & a_1+a_2+\cdots+ a_k\\
i \gets i + 1   & k+1 & a_1+a_2+\cdots+ a_k\\
\hline
\multicolumn{3}{c}{\cdots} \\
\hline
r \gets r + a_i & n & a_1+a_2+\cdots+ a_n\\
i \gets i + 1   & n+1 & a_1+a_2+\cdots+ a_n\\
\end{array}
$$

Lembre que o invariante é uma asserção que deve ser verdadeira cada vez que a
condição do laço é avaliada. Nestes pontos, observamos que
\begin{enumerate}
\item O valor de $i$ varia entre $1$ (inicial) e $n+1$ (quando a condição
  avaliada é falsa e o laço termina): 
  $$1 \le i \le n+1.$$
\item O valor de $r$ é a soma dos elementos do arranjo $A$ das posições $1$ até
  o valor de $i-1$:
  $$r = \sum_{k=1}^{i-1} a_k.$$
\end{enumerate}
Ou seja, chegamos à seguinte conjectura:
\begin{eqnarray*}
\mathsf{P} & \equiv & 1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k
\end{eqnarray*}
Para verificar esta conjectura, nós resta verificar se cada uma das seguintes
fórmulas é válida (a última fórmula sendo uma tripla de Hoare):
\begin{align}
\left(n > 0 \land r = 0 \land i = 1\right) \Rightarrow \left(1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k\right) \label{po:pre} \\
\left(1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k \land \neg i \le n\right) \Rightarrow r = \sum_{k=1}^{n} a_k \label{po:pos} \\
\assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k \land i \le n} r \gets r + a_i; i \gets i+1 \assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k} \label{po:inv}
\end{align}

\paragraph{Prova de~\ref{po:pre}:} Para verificar que esta fórmula é válida,
devemos mostrar que, das hipóteses $n>0$, $r=0$ e $i=1$ podemos deduzir $1 \le i
\le n+1$ e $r = \sum_{k=1}^{i-1} a_k$. De fato, temos:
\begin{itemize}
\item de $n>0$ e $i=1$, podemos deduzir que $1 \le i$ e $i \le n+1$.
\item de $r = 0$ e $i=1$, $r = \sum_{k=1}^{i-1} a_k$ pode ser simplificado em $0
  = \sum_{k=1}^{0} a_k$; o termo direito é uma soma sobre um intervalo vazio,
  que, por definição, é $0$.
\end{itemize}
\paragraph{Prova de~\ref{po:pos}:} Desta vez, as hipóteses são
$1 \le i \le n+1$, $\neg i \le n$, e $r = \sum_{k=1}^{i-1} a_k$. Delas deduzimos
que $i = n+1$ e $r = \sum_{k=1}^{n} a_k$, justamente a conclusão desejada.
\paragraph{Prova de~\ref{po:inv}:} Primeiro, vamos imediatamente simplificar $1
\le i \le n+1 \land i \le n$ na pré-condição da tripla que desejamos provar.
Assim, queremos construir uma prova que a seguinte tripla de Hoare é válida:
\begin{align*}
\assert{1 \le i \le n \land r = \sum_{k=1}^{i-1} a_k} r \gets r + a_i; i \gets i+1 \assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k}
\end{align*}

É uma sequência de duas instruções, e introduzimos a asserção intermediária que permite aplicar a regra da sequência: $1 \le i \le n \land r = a_i + \sum_{k=1}^{i-1} a_k$, simplificando-se para $1 \le i \le n \land \sum_{k=1}^{i} a_k$.
A derivação que mostra a tripla válida portanto é:

\infer[^1]{\assert{1 \le i \le n \land r = \sum_{k=1}^{i-1} a_k} r \gets r + a_i; i \gets i+1 \assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k}}
{\infer[^2]{\footnotesize \assert{1 \le i \le n \land r = \sum_{k=1}^{i-1} a_k} r \gets r + a_i \assert{1 \le i \le n \land r = \sum_{k=1}^{i} a_k}}{} &
\infer[^3]{\begin{array}{l}
\assert{1 \le i \le n \land r = \sum_{k=1}^{i} a_k} \\
\quad i \gets i+1 \\
\assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k}
\end{array}}{}}
Vamos detalhar cada uma dessas inferências:
\begin{enumerate}
\item É uma aplicação da regra da sequência, logo é válida.
\item É uma aplicação da regra da atribuição, a qual é válida se e somente se
  $$\left(1 \le i \le n \land r = \sum_{k=1}^{i-1} a_k\right) \Leftrightarrow \left(1 \le i \le n \land r = \sum_{k=1}^{i} a_k\right)[r+a_i/r]$$
  for válido.

  Como $r+a_i = \sum_{k=1}^i a_k \Leftrightarrow r+a_i = \left(\sum_{k=1}^{i-1} a_k\right)+a_i$, isto é o caso.
\item Também é uma aplicação da regra da atribuição, e esta é válida se e somente
  se
  $$\left(1 \le i \le n \land r = \sum_{k=1}^{i} a_k\right) \Leftrightarrow \left(1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k\right)[i+1/i]$$
  for válido.

  No entanto:
  \begin{eqnarray*}
    \left(1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k\right)[i+1/i] & \Leftrightarrow & \left(1 \le i+1 \le n+1 \land r = \sum_{k=1}^i a_k\right) \\
    & \Leftrightarrow & \left(0 \le i \le n \land r = \sum_{k=1}^i a_k\right) \\
    & \not\Leftrightarrow & \left(1 \le i \le n \land r = \sum_{k=1}^i a_k\right).
  \end{eqnarray*}
  No entanto, embora não há equivalência lógica, tem uma relação de implicação
  (pois $1 \le i \le n \Rightarrow 0 \le i \le n$). Assim, para construir uma
  derivação correta, descobrimos que devemos novamente aplicar a regra do
  fortalecimento da pré-condição:

  \infer{\assert{1 \le i \le n \land r = \sum_{k=1}^{i} a_k} i \gets i+1 \assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k}}
  {\infer{\assert{0 \le i \le n \land r = \sum_{k=1}^{i} a_k} i \gets i+1 \assert{1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k}}{\left(1 \le i \le n+1 \land r = \sum_{k=1}^{i-1} a_k\right)[i+1/i] \Leftrightarrow 0 \le i \le n \land r = \sum_{k=1}^{i} a_k} & \begin{array}[b]{l}1 \le i \le n \land r = \sum_{k=1}^{i} a_k \\
    \Rightarrow 0 \le i \le n \land r = \sum_{k=1}^{i} a_k
    \end{array}}
\end{enumerate}

\section{Considerações finais}

Anotamos o algoritmo original com todas as asserções necessárias para mostrar
que é correto. Todas as triplas de Hoare formadas por essas asserções,
conjugadas com as instruções correspondentes foram mostradas válidas.

Para construir esta prova, foram aplicadas as regras de sequência, atribuição,
enfraquecimento da pós-condição, fortalecimento da pré-condição e do
laço. Também foi necessário simular simbolicamente a execução do laço para
encontrar o invariante do laço. Encontrar asserção é o ponto chave na resolução
do exercício; de forma geral, isto é o caso de todo algoritmo envolvendo uma
construção de repetição. Uma vez identificado o invariante, o resto dos passos
de prova geralmente são simples e podem ser realizados de forma quase automática
(de fato existem ferramentas que fazem isto).

\paragraph{Próximos passos.} Caso não tenha conseguido resolver este exercício,
a orientação é ler novamente a resolução proposta até entender cada detalhe e
tentar resolver novamente sem consulta. Uma vez que entendeu bem como resolver
este exercício, estude novamente nos slides o exemplo de prova de correção de
algoritmos exposto em sala de aula. Treine até ser capaz de resolver este
sem consulta também!

\paragraph{S.A.C.:}
Dúvidas podem ser encaminhadas para o email do autor
(\url{david@dimap.ufrn.br}).  Inevitáveis erros de digitação, redação,
etc. também podem ser assinalados através do mesmo canal de comunicação. De
forma geral, críticas construtivas sempre são importantes para aprimorar o material
e são sempre muito bem recebidas.

\end{document}